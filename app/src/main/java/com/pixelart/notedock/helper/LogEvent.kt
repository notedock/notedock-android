package com.pixelart.notedock.helper

enum class LogEventParams(val value: String) {
    USER_UID("userUID"),
    ERROR("error"),
}
