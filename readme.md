# NoteDock

NoteDock is a multiplatform application for saving simple notes and organizing them into folders. They are automatically synchronized across all platforms in real time.

## Platforms

* Android: App got pulled from Play Store and source code can be found [here](https://gitlab.com/notedock/notedock-android).
* iOS: App created but refused to be accepted into app store and source code can be found [here](https://gitlab.com/notedock/notedock-ios).
* Web: [NoteDock](https://notedock.firebaseapp.com/). Web page was created by [Martin Pilát](https://www.linkedin.com/in/martin-pil%C3%A1t-ab319021b/).

## Implementation specifics

Application uses Firebase Authentication with email and password. It checks if the email is verified to before allowing to log in.

Firebase Firestore is used as a database. Calls to Firestore are wrapped using RxKotlin/RxJava.

Application uses Fragments and Navigation graphs with MVVM. Koin is the library used for DI.r